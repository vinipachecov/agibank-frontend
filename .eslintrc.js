module.exports = {
  env: {
    browser: true,
    es6: true,
    node: true,
    jest: true,
  },

  extends: ['standard', 'plugin:react/recommended'],

  globals: {
    Atomics: 'readonly',
    SharedArrayBuffer: 'readonly',
  },
  parserOptions: {
    ecmaFeatures: {
      jsx: true,
    },
    ecmaVersion: 2018,
    sourceType: 'module',
  },
  plugins: ['react'],
  rules: {
    'react/jsx-uses-react': 'error',
    'react/jsx-uses-vars': 'error',
    'comma-dangle': 0,
    'space-before-function-paren': 0,
    'react/react-in-jsx-scope': 0,
    camelcase: 0,
  },
  parser: 'babel-eslint',
}
