import styled from 'styled-components'
import theme from './theme'

export const H4 = styled.h4`
  font-size: ${theme.fontSize.H4};
  font-weight: ${theme.fontWeight.REGULAR};
`
