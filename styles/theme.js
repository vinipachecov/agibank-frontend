const theme = {
  colors: {
    yellow: '#ffe300',
    blue: '#1c1e22',
  },
  fontSize: {
    H1: '35px',
    H2: '30px',
    H3: '26px',
    H4: '20px',
    H5: '16px',
    H6: '14px',
    H7: '12px',
  },
  fontWeight: {
    THIN: '300',
    REGULAR: '400',
    BOLD: '600',
  },
}

export default theme
