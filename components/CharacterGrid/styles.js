import styled from 'styled-components'
import { H4 } from '../../styles/styled'

export const Container = styled.div`
  width: 100%;
  padding: 0 30px 30px 30px;
`

export const Title = styled(H4)`
  margin: 30px 0px;
`

export const Grid = styled.div`
  display: grid;
  grid-template-columns: repeat(auto-fit, minmax(80px, auto));
  grid-row-gap: 15px;
  grid-column-gap: 15px;
`

export const CharacterElement = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  text-align: center;
  box-shadow: 2px 2px 2px 2px rgba(0, 0, 0, 0.3);
  vertical-align: middle;
  height: 80px;
`
