import React from 'react'
import { render } from '@testing-library/react'
import CharacterGrid from './index'

describe('<CharacterGrid />', () => {
  const props = {
    characters: [
      {
        id: 1,
        name: 'johnny',
      },
      {
        id: 2,
        name: 'Leia',
      },
    ],
  }
  it('should render <CharacterGrid />', () => {
    const { getByText } = render(<CharacterGrid {...props} />)
    for (const character of props.characters) {
      expect(getByText(character.name)).toBeInTheDocument()
    }
    expect(getByText('Personagens')).toBeInTheDocument()
  })
})
