import React from 'react'
import PropTypes from 'prop-types'
import * as styles from './styles'

function CharacterGrid({ characters }) {
  return (
    <styles.Container>
      <styles.Title>Personagens</styles.Title>
      <styles.Grid>
        {characters.map(character => (
          <styles.CharacterElement key={character.id}>
            {character.name}
          </styles.CharacterElement>
        ))}
      </styles.Grid>
    </styles.Container>
  )
}

CharacterGrid.propTypes = {
  characters: PropTypes.array.isRequired,
}

export default CharacterGrid
