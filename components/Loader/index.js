import React from 'react'
import { Spinner } from './styles'

export default function Loader() {
  return <Spinner />
}
