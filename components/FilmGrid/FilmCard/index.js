import React from 'react'
import PropTypes from 'prop-types'
import Link from 'next/link'
import * as styles from './styles'
import { format } from 'date-fns'
import { truncateString } from '../../../helpers'

export function FilmCard(props) {
  const { id, title, opening_crawl, release_date } = props
  return (
    <>
      <Link href={`/films/${id}`}>
        <styles.CardContainer>
          <styles.Picture>
            <styles.FilmImage src={`/static/imgs/films/${id}.jpg`} />
          </styles.Picture>
          <styles.FilmContent>
            <styles.CardHeader>
              <styles.Title>{title}</styles.Title>
            </styles.CardHeader>
            <styles.YearReleased>
              {format(new Date(release_date), 'dd/MM/yyyy')}
            </styles.YearReleased>
            <styles.Description>
              {truncateString(opening_crawl)}
            </styles.Description>
          </styles.FilmContent>
        </styles.CardContainer>
      </Link>
    </>
  )
}

FilmCard.propTypes = {
  id: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  opening_crawl: PropTypes.string.isRequired,
  director: PropTypes.string.isRequired,
  producer: PropTypes.string.isRequired,
  release_date: PropTypes.string.isRequired,
}
