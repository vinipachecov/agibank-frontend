import styled from 'styled-components'
import theme from '../../../styles/theme'

export const CardContainer = styled.div`
  font-size: 12px;
  padding: 20px 30px;
  display: flex;
  flex-direction: row;
  justify-content: center;
  width: 100%;
  height: 350px;
  cursor: pointer;
  overflow: hidden;
  box-shadow: 0 2px 8px rgba(0, 0, 0, 0.1);
`

export const CardHeader = styled.div`
  display: flex;
  align-items: flex-end;
`

export const Title = styled.div`
  font-size: ${theme.fontSize.H4};
  font-weight: ${theme.fontWeight.BOLD};
  margin-right: 5%;
  width: 100%;
`

export const Director = styled.div`
  font-size: ${theme.fontSize.H5};
  margin-right: 5%;
`

export const Producer = styled.div`
  font-size: ${theme.fontSize.H5};
  margin-right: 5%;
`

export const YearReleased = styled.div`
  font-size: ${theme.fontSize.H5};
  margin-right: 5%;
`

export const Description = styled.div`
  font-size: ${theme.fontSize.H7};
  margin-top: 20px;
  text-align: justify;
`

export const Picture = styled.picture`
  max-width: 200px;
  max-height: 100%;
  margin-right: 3%;
`

export const FilmImage = styled.img`
  height: 100%;
  width: 100%;
`

export const FilmContent = styled.div`
  width: 60%;
  height: 100%;
`
