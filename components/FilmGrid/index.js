import React from 'react'
import PropTypes from 'prop-types'
import * as styles from './styles'
import { FilmCard } from './FilmCard'

export function FilmGrid({ films }) {
  console.log(films)
  return (
    <styles.Container>
      {films.map(film => (
        <FilmCard key={film.id} {...film} />
      ))}
    </styles.Container>
  )
}

FilmGrid.propTypes = {
  films: PropTypes.array.isRequired,
}
