import styled from 'styled-components'

export const Container = styled.div`
  margin-top: 60px;
  width: 100%;
  display: grid;
  grid-template-columns: repeat(2, 1fr);
  grid-template-rows: repeat(2, 1fr);
  grid-row-gap: 60px;
  grid-column-gap: 60px;
  padding: 0px 30px;

  @media (max-width: 768px) {
    grid-template-columns: repeat(1, 1fr);
  }
`
