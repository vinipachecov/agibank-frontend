import React from 'react'
import { render } from '@testing-library/react'
import { Header } from './index'

describe('<Header />', () => {
  it('should render Header', () => {
    const { getByText } = render(<Header />)
    expect(getByText('Teste Agibank')).toBeInTheDocument()
  })
})
