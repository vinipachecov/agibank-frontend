import styled from 'styled-components'
import theme from '../../styles/theme'

export const Container = styled.div`
  background: black;
  width: 100%;
  height: 68px;
  display: flex;
  justify-content: center;
  align-items: center;
`

export const Title = styled.div`
  font-size: ${theme.fontSize.H1};
  cursor: pointer;
  color: ${theme.colors.yellow};
  font-weight: ${theme.fontWeight.BOLD};
`
