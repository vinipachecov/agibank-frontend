import React from 'react'
import Link from 'next/link'
import * as styles from './styles'

export function Header() {
  return (
    <styles.Container>
      <Link href="/">
        <styles.Title>Teste Agibank</styles.Title>
      </Link>
    </styles.Container>
  )
}
