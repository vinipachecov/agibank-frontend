import { truncateString } from './index'
describe('Helpers', () => {
  it('Truncate in 300', () => {
    const phrase = 'a'.repeat(304)
    const trucated = truncateString(phrase)
    expect(trucated.length).toEqual(303)
    expect(trucated[302]).toEqual('.')
    expect(trucated[301]).toEqual('.')
    expect(trucated[300]).toEqual('.')
  })
})
