import React, { useEffect, useState } from 'react'
import { FilmCard } from '../../components/FilmGrid/FilmCard'
import Router from 'next/router'
import FilmService from '../../services/FilmService'
import CharacterGrid from '../../components/CharacterGrid'
import Loader from '../../components/Loader'

export function FilmDetails() {
  const [film, setFilm] = useState({})

  const getFilm = async id => {
    try {
      const film = await FilmService.getFilmById(id)

      setFilm(film)
    } catch ({ response: { status } }) {
      switch (status) {
        case 404:
          alert('Filme não encontrado.')
          break
        default:
          alert('Erro ao buscar dados no servidor.')
      }
    }
  }
  useEffect(() => {
    getFilm(Router.query.id)
  }, [])

  return (
    <>
      {JSON.stringify(film) === JSON.stringify({}) ? (
        <Loader />
      ) : (
        <>
          <FilmCard {...film} />
          <CharacterGrid characters={film.characters} />
        </>
      )}
    </>
  )
}

export default FilmDetails
