import React, { useEffect, useState } from 'react'
import Loader from '../components/Loader'
import FilmService from '../services/FilmService'
import { FilmGrid } from '../components/FilmGrid'

function Home() {
  const [films, setFilms] = useState([])

  const fetchMovies = async () => {
    try {
      const films = await FilmService.getFilms()
      setFilms(films)
    } catch (error) {
      alert('Não foi possível buscar filmes =/')
    }
  }

  useEffect(() => {
    fetchMovies()
  }, [])

  return <>{films.length > 0 ? <FilmGrid films={films} /> : <Loader />}</>
}

export default Home
