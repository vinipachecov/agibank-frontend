import axios from 'axios'

export default class BaseService {
  service = axios.create({
    baseURL: 'https://swapi.co/api/',
  })
}
