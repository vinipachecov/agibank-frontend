import BaseService from './BaseService'
import axios from 'axios'

class CharacterService extends BaseService {
  async getCharacterByUrl(url) {
    const splitUrl = url.split('/')
    const response = await axios.get(url)
    return {
      ...response.data,
      id: splitUrl[splitUrl.length - 2],
    }
  }

  async getCharacterById(id) {
    const response = await this.service.get(`/people/${id}`)
    const split = response.data.url.split('/')
    return {
      ...response.data,
      id: split[split.length - 2],
    }
  }
}

export default new CharacterService()
