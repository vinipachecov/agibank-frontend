import BaseService from './BaseService'
import CharacterService from './CharacterService'

class FilmService extends BaseService {
  async getFilms() {
    const response = await this.service.get('/films')

    return response.data.results.map(film => {
      const splitMovieId = film.url.split('/')
      return {
        ...film,
        id: splitMovieId[splitMovieId.length - 2],
      }
    })
  }

  async getFilmById(id) {
    const { data } = await this.service.get(`/films/${id}`)

    const characters = await Promise.all([
      ...data.characters.map(character =>
        CharacterService.getCharacterByUrl(character)
      ),
    ])

    const split = data.url.split('/')
    return {
      ...data,
      id: split[split.length - 2],
      characters: characters,
    }
  }
}

export default new FilmService()
