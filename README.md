<h1>Sobre</h1>

Este repositório é sobre o desafio de front-end de construir um app em react que possa consumir uma API com dados do starwars, mais espeficiamente: https://swapi.co/

<h1>Stack</h1>

- React (com hooks)
- Next.js
- Styled-components
- Axios
- Eslint js-standard
- Gitlab CI
- Now
- Jest
- Babel
- Prettier

<h1>Pastas</h1>
A estrutura de pastas utilizadas foi a seguinte:

- pages (Páginas)
- styles (estilos e temas)
- hoc (high order components)
- components (components das pages)
- services (chamadas a APIs externas)

<h1>Páginas</h1>
As páginas existentes no projeto são:
- Filmes (/)
- Detalhe de um Filme(/film/id)

<h1>Como rodar</h1>

<h4>Desenvolvimento</h4>
Para rodar em modo de desenvolvimento:
```
npm run dev
```

Iniciará um servidor em localhost:3000

<h4>Testes</h4>
Para rodar a bateria de testes:
```
npm run test
```

Em produção

https://agibank-frontend.vinipachecov.now.sh/
