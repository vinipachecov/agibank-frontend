module.exports = {
  setupFilesAfterEnv: ['<rootDir>/jest.setup.js'],
  testPathIgnorePatterns: ['<rootDir>/.next/', '<rootDir>/node_modules/'],
  testMatch: ['**/src/**/*.[jt]s?(x)', '**/?(*.)+(spec|test).[jt]s?(x)'],
}
