import React from 'react'
import PropTypes from 'prop-types'
import * as styles from './styles'
import { Header } from '../../components/Header'

export function Layout(props) {
  return (
    <styles.Container>
      <Header />
      {props.children}
    </styles.Container>
  )
}

Layout.propTypes = {
  children: PropTypes.node,
}
