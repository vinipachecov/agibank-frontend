import React from 'react'
import { render } from '@testing-library/react'
import { Layout } from './index'

describe('<Layout />', () => {
  it('should render Layout children ', () => {
    const { getByText } = render(
      <Layout>
        <div>HERE</div>
      </Layout>
    )
    expect(getByText('HERE')).toBeInTheDocument()
  })
})
